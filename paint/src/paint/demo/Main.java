package paint.demo;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingUtilities;

import paint.graphics.model.GraphicalObject;
import paint.graphics.model.LineSegment;
import paint.graphics.model.Oval;
import paint.gui.GUI;

public class Main {
	
	public static void main(String[] args) {

		List<GraphicalObject> objects = new ArrayList<>();

		objects.add(new LineSegment());
		objects.add(new Oval());
		
		SwingUtilities.invokeLater(() -> {
			GUI gui = new GUI(objects);
			gui.setVisible(true);			
		});
	}

}
