package paint.document.model;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import paint.graphics.model.GraphicalObject;
import paint.graphics.model.GraphicalObjectListener;
import paint.model.util.Point;

public class DocumentModel {

	public final static double SELECTION_PROXIMITY = 10;

	private List<GraphicalObject> objects = new ArrayList<>();
	private List<GraphicalObject> roObjects = Collections.unmodifiableList(objects);
	private List<DocumentModelListener> listeners = new ArrayList<>();
	private List<GraphicalObject> selectedObjects = new ArrayList<>();
	private List<GraphicalObject> roSelectedObjects = Collections.unmodifiableList(selectedObjects);


	private final GraphicalObjectListener goListener = new GraphicalObjectListener() {

		@Override
		public void graphicalObjectChanged(GraphicalObject go) {
			notifyListeners();
		}

		@Override
		public void graphicalObjectSelectionChanged(GraphicalObject go) {
			Objects.requireNonNull(go);
			
			if(go.isSelected() && !selectedObjects.contains(go)) selectedObjects.add(go);
			if(!go.isSelected()) selectedObjects.remove(go);
			
			notifyListeners();
		}
		
	};
	
	public DocumentModel() {
		
	}

	public void clear() {
		for(GraphicalObject o : objects) {
			o.removeGraphicalObjectListener(goListener);
		}
		
		objects.clear();
		selectedObjects.clear();
		
		notifyListeners();
	}


	public void addGraphicalObject(GraphicalObject obj) {
		Objects.requireNonNull(obj);
		
		obj.addGraphicalObjectListener(goListener);
		objects.add(obj);
		
		if(obj.isSelected()) selectedObjects.add(obj);
		
		notifyListeners();
	}
	
	public void removeGraphicalObject(GraphicalObject obj) {
		if(obj == null) return;
		
		obj.removeGraphicalObjectListener(goListener);
		objects.remove(obj);
		
		if(obj.isSelected()) selectedObjects.remove(obj);
		
		notifyListeners();
	}

	public List<GraphicalObject> list() {
		return roObjects;
	}

	public void addDocumentModelListener(DocumentModelListener l) {
		listeners.add(Objects.requireNonNull(l));
	}
	
	public void removeDocumentModelListener(DocumentModelListener l) {
		listeners.remove(l);
	}

	public void notifyListeners() {
		listeners.forEach(DocumentModelListener::documentChange);
	}
	
	public List<GraphicalObject> getSelectedObjects() {
		return roSelectedObjects;
	}

	public void increaseZ(GraphicalObject go) {
		Objects.requireNonNull(go);
		
		int index = objects.indexOf(go);
		if(index == -1 || index == objects.size() - 1) return;
		
		objects.remove(go);
		objects.add(index + 1, go);
		
		notifyListeners();
	}
	
	public void decreaseZ(GraphicalObject go) {
		Objects.requireNonNull(go);
		
		int index = objects.indexOf(go);
		if(index == -1 || index == 0) return;
		
		objects.remove(go);
		objects.add(index - 1, go);
		
		notifyListeners();
	}
	
	public GraphicalObject findSelectedGraphicalObject(Point mousePoint) {
		Objects.requireNonNull(mousePoint);
		
		GraphicalObject closest = null;
		double shortestDistance = -1;
		
		for (GraphicalObject graphicalObject : objects) {
			double distance = graphicalObject.selectionDistance(mousePoint);	
			
			if(shortestDistance < 0 || distance < shortestDistance) {
				shortestDistance = distance;
				closest = graphicalObject;
			}
		}
		
		if(shortestDistance > SELECTION_PROXIMITY) return null;
		
		return closest;
	}

	public int findSelectedHotPoint(GraphicalObject object, Point mousePoint) {
		Objects.requireNonNull(object);
		Objects.requireNonNull(mousePoint);
		
		double shortestDistance = -1;
		int closestIndex = -1;
		
		int numberOfHotPoints = object.getNumberOfHotPoints();
		for (int i = 0; i < numberOfHotPoints; i++) {
			double distance = object.getHotPointDistance(i, mousePoint);
			
			if(shortestDistance < 0 || distance < shortestDistance) {
				shortestDistance = distance;
				closestIndex = i;
			}
		}
		
		if(shortestDistance > SELECTION_PROXIMITY) return -1;
		
		return closestIndex;
	}

}