package paint.document.model;
public interface DocumentModelListener {

	public void documentChange();
	
}