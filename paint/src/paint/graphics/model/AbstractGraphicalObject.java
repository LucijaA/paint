package paint.graphics.model;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import paint.model.util.GeometryUtil;
import paint.model.util.Point;

public abstract class AbstractGraphicalObject  implements GraphicalObject {

	private Point[] hotPoints;
	private boolean[] hotPointSelected;
	private boolean selected;
	
	List<GraphicalObjectListener> listeners = new ArrayList<>();
	
	protected AbstractGraphicalObject(Point[] hotPoints) {
		this.hotPoints = hotPoints;
		hotPointSelected = new boolean[hotPoints.length];
	}
		
	@Override
	public void translate(Point delta) {
		for (int i = 0; i < hotPoints.length; i++) {
			setHotPoint(i,hotPoints[i].translate(delta));
		}
		
		notifyListeners();
	}

	@Override
	public boolean isSelected() {
		return selected;
	}

	@Override
	public void setSelected(boolean selected) {
		this.selected = selected;
		notifySelectionListeners();
	}

	@Override
	public int getNumberOfHotPoints() {
		return hotPoints.length;
	}

	@Override
	public Point getHotPoint(int index) {
		checkIndex(index, hotPoints.length);
		return hotPoints[index];
	}

	@Override
	public void setHotPoint(int index, Point point) {
		checkIndex(index, hotPoints.length);
		hotPoints[index] = Objects.requireNonNull(point);
		
		notifyListeners();
	}

	@Override
	public boolean isHotPointSelected(int index) {
		checkIndex(index, hotPointSelected.length);
		
		return hotPointSelected[index];
	}

	@Override
	public void setHotPointSelected(int index, boolean selected) {
		checkIndex(index, hotPointSelected.length);
		
		for (int i = 0; i < hotPointSelected.length; i++) {
			if(i == index) hotPointSelected[index] = selected;
			
			else if(isHotPointSelected(i)) setHotPointSelected(i, false);
		}
		
		notifyListeners();
	}

	@Override
	public double getHotPointDistance(int index, Point mousePoint) {
		checkIndex(index, hotPoints.length);
		Objects.requireNonNull(mousePoint);
		
		return GeometryUtil.distanceFromPoint(hotPoints[index], mousePoint);
	}

	@Override
	public void addGraphicalObjectListener(GraphicalObjectListener l) {
		listeners.add(Objects.requireNonNull(l));
	}

	@Override
	public void removeGraphicalObjectListener(GraphicalObjectListener l) {
		listeners.remove(l);
	}


	protected void notifyListeners() {
		listeners.forEach(l -> l.graphicalObjectChanged(this));
	}
	
	protected void notifySelectionListeners() {
		listeners.forEach(l -> l.graphicalObjectSelectionChanged(this));
	}
	
	private void checkIndex(int index, int len) {
		if(index < 0 || index >= len) throw new IllegalArgumentException();
	}
	
	@Override
	public boolean isComposite() {
		return false;	
	}
}
