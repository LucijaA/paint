package paint.graphics.model;

import paint.model.rendering.Renderer;

public interface Drawable {
	
	// Podrška za crtanje (dio mosta)
	public void render(Renderer r);

}
