package paint.graphics.model;
import paint.model.util.Point;
import paint.model.util.Rectangle;

public interface GraphicalObject extends Prototype, Drawable, Writeble {

	// Podrška za uređivanje objekta
	public boolean isSelected();
	public void setSelected(boolean selected);
	public int getNumberOfHotPoints();
	public Point getHotPoint(int index);
	public void setHotPoint(int index, Point point);
	public boolean isHotPointSelected(int index);
	public void setHotPointSelected(int index, boolean selected);
	public double getHotPointDistance(int index, Point mousePoint);
	
	// Observer za dojavu promjena modelu
	public void addGraphicalObjectListener(GraphicalObjectListener l);
	public void removeGraphicalObjectListener(GraphicalObjectListener l);
	
	// Geometrijska operacija nad oblikom
	public void translate(Point delta);
	public Rectangle getBoundingBox();
	public double selectionDistance(Point mousePoint);
	
	public boolean isComposite();
	
}