package paint.graphics.model;
import java.util.List;
import java.util.Objects;
import java.util.Stack;

import paint.model.rendering.Renderer;
import paint.model.util.GeometryUtil;
import paint.model.util.Point;
import paint.model.util.Rectangle;

public class LineSegment extends AbstractGraphicalObject {

	private String name = "Linija";
	
	public LineSegment() {
		super(new Point[]{
				new Point(0, 0),
				new Point(10, 0)
				});
	}
	
	public LineSegment(Point start, Point end) {
		super(new Point[] {
				Objects.requireNonNull(start),
				Objects.requireNonNull(end)
				});
	}

	@Override
	public Rectangle getBoundingBox() {
		int xmin = Math.min(getHotPoint(0).getX(), getHotPoint(1).getX());
		int ymin = Math.min(getHotPoint(0).getY(), getHotPoint(1).getY());
		
		int dx = Math.abs(getHotPoint(1).getX() - getHotPoint(0).getX());
		int dy = Math.abs(getHotPoint(1).getY() - getHotPoint(0).getY());
		
		
		return new Rectangle(xmin, ymin, dx, dy);
	}

	@Override
	public double selectionDistance(Point mousePoint) {
		return GeometryUtil.distanceFromLineSegment(getHotPoint(0), getHotPoint(1), Objects.requireNonNull(mousePoint));
	}

	@Override
	public String getShapeName() {
		return name;
	}

	@Override
	public GraphicalObject duplicate() {
		return new LineSegment(getHotPoint(0), getHotPoint(1));
	}

	@Override
	public void render(Renderer r) {
		r.drawLine(getHotPoint(0), getHotPoint(1));
	}

	@Override
	public String getShapeID() {
		return "@LINE";
	}

	@Override
	public void load(Stack<GraphicalObject> stack, String data) {
		Objects.requireNonNull(stack);
		Objects.requireNonNull(data);
		
		String[] points = data.trim().split("\\s+");
		
		Point start = new Point(Integer.parseInt(points[0]), Integer.parseInt(points[1]));
		Point end = new Point(Integer.parseInt(points[2]), Integer.parseInt(points[3]));
		
		stack.push(new LineSegment(start, end));
	}

	@Override
	public void save(List<String> rows) {
		StringBuilder sb = new StringBuilder();
		
		sb.append(getShapeID()).append(" ")
		.append(getHotPoint(0).getX()).append(" ").append(getHotPoint(0).getY()).append(" ")
		.append(getHotPoint(1).getX()).append(" ").append(getHotPoint(1).getY());
		
		rows.add(sb.toString());
	}

}
