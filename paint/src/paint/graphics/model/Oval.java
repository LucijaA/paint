package paint.graphics.model;
import java.util.List;
import java.util.Objects;
import java.util.Stack;

import paint.model.rendering.Renderer;
import paint.model.util.GeometryUtil;
import paint.model.util.Point;
import paint.model.util.Rectangle;

public class Oval extends AbstractGraphicalObject {

	private String name = "Oval";
	private Point center;
	
	public Oval() {
		super(new Point[] {
				new Point(10, 0),
				new Point(0, 10)
		});
		
		center = new Point(0, 0);
	}
	
	public Oval(Point right, Point low) {
		super(new Point[] {
				Objects.requireNonNull(right),
				Objects.requireNonNull(low)
		});
		
		center = new Point(low.getX(), right.getY());
	}

	@Override
	public Rectangle getBoundingBox() {
		int xmin = center.getX() - Math.abs(getHotPoint(0).getX() - center.getX());
		int ymin = center.getY() - Math.abs(getHotPoint(1).getY() - center.getY());
		
		return new Rectangle(xmin, ymin, 2 * Math.abs(getHotPoint(0).getX() - center.getX()), 2 * Math.abs(getHotPoint(1).getY() - center.getY()));
	}

	@Override
	public double selectionDistance(Point mousePoint) {
		double distFromCenter = GeometryUtil.distanceFromPoint(center, mousePoint);
		double b = GeometryUtil.distanceFromPoint(center, getHotPoint(1));
		double a = GeometryUtil.distanceFromPoint(center, getHotPoint(0));
		
		Point diff = mousePoint.difference(center);
		
		double k = (double)diff.getY() / diff.getX();
		
		if(Math.abs(k - Math.PI/2) < 10E-6) return Math.min(
				GeometryUtil.distanceFromPoint(getHotPoint(1), mousePoint),
				GeometryUtil.distanceFromPoint(center.difference(getHotPoint(1)), mousePoint)
				);
		
		double alpha = Math.atan(k);
		double x = a * Math.cos(alpha) + center.getX();
		double y = b * Math.sin(alpha) + center.getY();
		
		Point ovalPoint = new Point((int)Math.round(x), (int)Math.round(y));
		
		if(distFromCenter < GeometryUtil.distanceFromPoint(center, ovalPoint)) return 0.0;
		
		return GeometryUtil.distanceFromPoint(ovalPoint, mousePoint);
	}

	@Override
	public String getShapeName() {
		return name;
	}

	@Override
	public GraphicalObject duplicate() {
		return new Oval(getHotPoint(0), getHotPoint(1));
	}

	@Override
	public void render(Renderer r) {
		double b = GeometryUtil.distanceFromPoint(center, getHotPoint(1));
		double a = GeometryUtil.distanceFromPoint(center, getHotPoint(0));
		
		int n = 180;
		
		Point[] points = new Point[n];
		
		for (int i = 0; i < n; i++) {
			double alpha = 2 * i * Math.PI / n;
			int x = (int) Math.round(a * Math.cos(alpha)) + center.getX();
			int y = (int) Math.round(b * Math.sin(alpha)) + center.getY();
			
			points[i] = new Point(x, y);
		}
		
		
 		r.fillPolygon(points);
	}
	
	@Override
	public void translate(Point delta) {
		super.translate(delta);
		center = new Point(getHotPoint(1).getX(), getHotPoint(0).getY());
	}
	
	@Override
	public void setHotPoint(int index, Point point) {
		super.setHotPoint(index, point);
		center = new Point(getHotPoint(1).getX(), getHotPoint(0).getY());
	}

	@Override
	public String getShapeID() {
		return "@OVAL";
	}

	@Override
	public void load(Stack<GraphicalObject> stack, String data) {
		Objects.requireNonNull(data);
		Objects.requireNonNull(stack);
		
		String[] points = data.trim().split("\\s+");
		
		Point right = new Point(Integer.parseInt(points[0]), Integer.parseInt(points[1]));
		Point low = new Point(Integer.parseInt(points[2]), Integer.parseInt(points[3]));
		
		stack.push(new Oval(right, low));
	}

	@Override
	public void save(List<String> rows) {
		StringBuilder sb = new StringBuilder();
		
		sb.append(getShapeID()).append(" ")
		.append(getHotPoint(0).getX()).append(" ").append(getHotPoint(0).getY()).append(" ")
		.append(getHotPoint(1).getX()).append(" ").append(getHotPoint(1).getY());
		
		rows.add(sb.toString());
	}

}
