package paint.graphics.model;
public interface Prototype {
	
	// Podrška za prototip (alatna traka, stvaranje objekata u crtežu, ...)
	public String getShapeName();
	public GraphicalObject duplicate();

}
