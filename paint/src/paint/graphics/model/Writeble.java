package paint.graphics.model;
import java.util.List;
import java.util.Stack;

public interface Writeble {
	
	// Podrška za snimanje i učitavanje
	public String getShapeID();
	public void load(Stack<GraphicalObject> stack, String data);
	public void save(List<String> rows);

}
