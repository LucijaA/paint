package paint.gui;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JComponent;

import paint.document.model.DocumentModel;
import paint.model.rendering.G2DRendererImpl;
import paint.model.rendering.Renderer;
import paint.state.State;

public class Canvas extends JComponent {

	private static final long serialVersionUID = 1L;

	private DocumentModel document;
	private State currentState;

	public Canvas(DocumentModel document, State currentState) {
		super();
		this.document = document;
		this.currentState = currentState;

	}
	
	
	public void setCurrentState(State currentState) {
		this.currentState = currentState;
	}

	@Override
	protected void paintComponent(Graphics g) {
		Renderer r = new G2DRendererImpl((Graphics2D)g);
		document.list().forEach( go -> {
			go.render(r);
			currentState.afterDraw(r, go);
		});
		
		currentState.afterDraw(r);
	}
}
