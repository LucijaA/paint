package paint.gui;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Stack;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;

import paint.document.model.DocumentModel;
import paint.document.model.DocumentModelListener;
import paint.graphics.model.GraphicalObject;
import paint.model.rendering.SVGRendererImpl;
import paint.model.util.Point;
import paint.state.AddShapeState;
import paint.state.CompositeShape;
import paint.state.EraserState;
import paint.state.IdleState;
import paint.state.SelectShapeState;
import paint.state.State;

public class GUI extends JFrame {

	private static final long serialVersionUID = 1L;
	
	private List<GraphicalObject> objects;
	private DocumentModel doc = new DocumentModel();
	private State currentState = new IdleState();
	private Canvas canvas;
	private boolean shiftDown = false;
	private boolean ctrlDown = false;
	private Map<String, GraphicalObject> ids = new HashMap<>();

	public GUI(List<GraphicalObject> objects) {
		this.objects = Objects.requireNonNull(objects);
		
		objects.forEach(o -> ids.put(o.getShapeID(), o));
		
		CompositeShape cs = new CompositeShape(null);
		ids.put(cs.getShapeID(), cs);
		
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setSize(500, 500);
		setFocusable(true);
		
		doc.addDocumentModelListener(new DocumentModelListener() {
			
			@Override
			public void documentChange() {
				canvas.repaint();
				
			}
		});
		initGUI();
	}

	private void initGUI() {
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		
		createToolbar();
		
		canvas = new Canvas(doc, currentState);
		cp.add(BorderLayout.CENTER, canvas);
		
		addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseDragged(MouseEvent e) {
				currentState.mouseDragged(new Point(e.getPoint().x, e.getPoint().y - 60));
			};
		});
		
		addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if(e.getButton() == MouseEvent.BUTTON1) 
					currentState.mouseDown(new Point(e.getX(), e.getY() - 60), shiftDown, ctrlDown);
			}
			
			@Override
			public void mouseReleased(MouseEvent e) {
				if(e.getButton() == MouseEvent.BUTTON1) 
					currentState.mouseUp(new Point(e.getX(), e.getY() - 60), shiftDown, ctrlDown);
			}
			
		});
		
		addKeyListener(new KeyAdapter() {
			
			@Override
			public void keyPressed(KeyEvent e) {
				
				switch(e.getKeyCode()) {
				case KeyEvent.VK_ESCAPE:
					currentState.onLeaving();
					currentState = new IdleState();
					e.consume();
					break;
				case KeyEvent.VK_CONTROL:
					ctrlDown = true;
					e.consume();
					break;
				case KeyEvent.VK_SHIFT:
					shiftDown = true;
					e.consume();
					break;
				default:
					currentState.keyPressed(e.getKeyCode());
				}
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				switch(e.getKeyCode()) {
				case KeyEvent.VK_CONTROL:
					ctrlDown = false;
					e.consume();
					break;
				case KeyEvent.VK_SHIFT:
					shiftDown = false;

				}
			}
		});
	}

	private void createToolbar() {
		JToolBar tb = new JToolBar();
		tb.setFloatable(true);
		
		getContentPane().add(BorderLayout.PAGE_START, tb);
		
		for (GraphicalObject graphicalObject : objects) {
			StateButton button = new StateButton(graphicalObject, graphicalObject.getShapeName());
			button.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					currentState.onLeaving();
					currentState = new AddShapeState(doc, button.getGoraphicalObject());
					canvas.setCurrentState(currentState);
				}
			});
			tb.add(button);
			button.setFocusable(false);
		}
		
		JButton select = new JButton(new AbstractAction() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				currentState.onLeaving();
				currentState = new SelectShapeState(doc);
				canvas.setCurrentState(currentState);

			}
		});
		
		select.setText("Selektiraj");
		select.setFocusable(false);
		tb.add(select);
		tb.setFocusable(false);
		
		JButton delete = new JButton(new AbstractAction() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				currentState.onLeaving();
				currentState = new EraserState(doc);
				canvas.setCurrentState(currentState);
				
			}
		});
		
		delete.setText("Brisalo");
		delete.setFocusable(false);
		tb.add(delete);
		
		JButton svgRenderer = new JButton(new AbstractAction() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				String fileName = getFilename();
				if(fileName == null) return;

				SVGRendererImpl r = new SVGRendererImpl(fileName);
				doc.list().forEach(o -> o.render(r));
				
				try {
					r.close();
				} catch (IOException e1) {
					error("An error occured while writing to file!");
				}
			}
		});
		
		svgRenderer.setText("SVG export");
		svgRenderer.setFocusable(false);
		tb.add(svgRenderer);
		
		JButton save = new JButton(new AbstractAction() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				String filename = getFilename();
				if(filename == null) return;
				
				List<String> rows = new ArrayList<>();
				doc.list().forEach(o -> o.save(rows));
				
				Path dest = Paths.get(filename);
				try {
					Files.write(dest, rows, StandardOpenOption.CREATE);
				} catch (IOException e1) {
					error("An error occured while writing to file!");
				}
				
			}
		});
		
		save.setText("Pohrani");
		save.setFocusable(false);
		tb.add(save);
		
		JButton load = new JButton(new AbstractAction() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser jfc = new JFileChooser();
				jfc.setDialogTitle("Select destination file");
				
				if(jfc.showOpenDialog(GUI.this) != JFileChooser.APPROVE_OPTION) return;
				
				Path source = jfc.getSelectedFile().toPath();
				List<String> lines;
				
				try {
					lines = Files.readAllLines(source);
				} catch (IOException e1) {
					error("An error occured while reading from file!");
					return;
				}
				
				Stack<GraphicalObject> stack = new Stack<>();
				extractObjects(lines, stack);
				
				while(!stack.empty()) {
					doc.addGraphicalObject(stack.pop());
				}
			}

		});
		
		load.setText("Učitaj");
		load.setFocusable(false);
		tb.add(load);
	}

	private String getFilename() {
		JFileChooser jfc = new JFileChooser();
		jfc.setDialogTitle("Select destination file");
		
		if(jfc.showSaveDialog(this) != JFileChooser.APPROVE_OPTION) return null;
		
		return jfc.getSelectedFile().toString();	
	}

	private void error(String message) {
		JOptionPane.showMessageDialog(this, message, "Error", JOptionPane.ERROR_MESSAGE);
	}
	
	private void extractObjects(List<String> lines, Stack<GraphicalObject> stack) {
		for (String line : lines) {
			String id = line.split("\\s+")[0];
			if(!ids.containsKey(id)) continue;
			
			ids.get(id).load(stack, line.substring(id.length() + 1));
		}
	}

}
