package paint.gui;

import javax.swing.JButton;

import paint.graphics.model.GraphicalObject;

public class StateButton extends JButton {

	private static final long serialVersionUID = 1L;
	
	private GraphicalObject go;

	public StateButton(GraphicalObject object, String name) {
		super(name);
		this.go = object;
	}

	public GraphicalObject getGoraphicalObject() {
		return go;
	}
	
}
