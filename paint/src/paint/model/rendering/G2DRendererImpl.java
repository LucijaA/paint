package paint.model.rendering;
import java.awt.Color;
import java.awt.Graphics2D;
import java.util.Objects;

import paint.model.util.Point;

public class G2DRendererImpl implements Renderer {

	private Graphics2D g2d;
	
	public G2DRendererImpl(Graphics2D g2d) {
		this.g2d = Objects.requireNonNull(g2d);
	}

	@Override
	public void drawLine(Point s, Point e) {
		Color old = g2d.getColor();
		
		g2d.setColor(Color.BLUE);
		g2d.drawLine(s.getX(), s.getY(), e.getX(), e.getY());
		
		g2d.setColor(old);
	}

	@Override
	public void fillPolygon(Point[] points) {
		Color old = g2d.getColor();
		
		g2d.setColor(Color.BLUE);
		
		int size = points.length;
		int[] xPoints = new int[size];
		int[] yPoints = new int[size];
 		
		for (int i = 0; i < size; i++) {
			xPoints[i] = points[i].getX();
			yPoints[i] = points[i].getY();
		}
		
		g2d.fillPolygon(xPoints, yPoints, size);
		
		g2d.setColor(Color.RED);
		g2d.drawPolygon(xPoints, yPoints, size);
		
		g2d.setColor(old);
	}

}