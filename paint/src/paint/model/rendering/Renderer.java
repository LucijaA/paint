package paint.model.rendering;
import paint.model.util.Point;

public interface Renderer {
	public void drawLine(Point s, Point e);
	public void fillPolygon(Point[] points);
}