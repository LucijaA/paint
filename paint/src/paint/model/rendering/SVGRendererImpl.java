package paint.model.rendering;
import java.awt.Color;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import paint.model.util.Point;

public class SVGRendererImpl implements Renderer {
	
	public static final String QUOTE = "\""; 
	public static final String SPACE = " "; 
	public static Color FILL = Color.BLUE;
	public static Color LINE = Color.RED;

	private List<String> lines = new ArrayList<>();
	private String fileName;
	
	public SVGRendererImpl(String fileName) {
		this.fileName = Objects.requireNonNull(fileName);
		lines.add("<svg  xmlns=\"http://www.w3.org/2000/svg\"\r\n" + 
				"      xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
	}

	public void close() throws IOException {
		lines.add("</svg>");
		
		Path dest = Paths.get(fileName);
		Files.write(dest, lines, StandardOpenOption.CREATE);
	}
	
	@Override
	public void drawLine(Point s, Point e) {
		
		StringBuilder sb = new StringBuilder();
		sb.append("<line ")
		.append("x1=").append(QUOTE).append(s.getX()).append(QUOTE).append(SPACE)
		.append("y1=").append(QUOTE).append(s.getY()).append(QUOTE).append(SPACE)
		.append("x2=").append(QUOTE).append(e.getX()).append(QUOTE).append(SPACE)
		.append("y2=").append(QUOTE).append(e.getY()).append(QUOTE).append(SPACE)
		.append("style=").append(QUOTE)
		.append("stroke:").append("rgb(")
		.append(FILL.getRed()).append(",").append(FILL.getGreen()).append(",").append(FILL.getBlue()).append(");")
		.append("stroke-width:").append(1)
		.append(QUOTE).append(SPACE)
		.append("/>");
		
		lines.add(sb.toString());
	}

	@Override
	public void fillPolygon(Point[] points) {
		StringBuilder sb = new StringBuilder();
		sb.append("<polygon points=").append(QUOTE);
		
		for (int i = 0, len = points.length; i < len; i++) {
			sb.append(points[i].getX()).append(",").append(points[i].getY());
			if(i != len - 1) sb.append(SPACE);
			else sb.append(QUOTE).append(SPACE);
		}
		
		sb.append("style=").append(QUOTE)
		.append("fill:").append("blue").append(";")
		.append("stroke:").append("red").append(";")
		.append("stroke-width:").append(1).append(QUOTE)
		.append(" />");
		
		lines.add(sb.toString());
	}

}