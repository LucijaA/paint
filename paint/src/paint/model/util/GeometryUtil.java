package paint.model.util;

import static java.lang.Math.*;

public class GeometryUtil {

	public static double distanceFromPoint(Point point1, Point point2) {
		int dx = point1.getX() - point2.getX();
		int dy = point1.getY() - point2.getY();
		
		return sqrt(dx * dx + dy * dy);
	}
	
	public static double distanceFromLineSegment(Point s, Point e, Point p) {
		
		int xMin = min(s.getX(), e.getX());
		int xMax = max(s.getX(), e.getX());
		
		int yMax = max(s.getY(), e.getY());
		int yMin = min(s.getY(), e.getY());
		
		if(p.getX() >= xMin && p.getX() <= xMax ||  p.getY() >= yMin && p.getY() <= yMax) {
			double a = s.getY() - e.getY();
			double b = -(s.getX() - e.getX());
			double c = -s.getX() * (s.getY() - e.getY()) + s.getY() * (s.getX() - e.getX());
			
			double norm = sqrt(a*a + b*b);
			
			a /= norm;
			b /= norm;
			c /= norm;
			
			return abs(a * p.getX() + b * p.getY() + c);
		} 
		
		return min(distanceFromPoint(s, p), distanceFromPoint(e, p));
	}
}