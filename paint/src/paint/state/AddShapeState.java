package paint.state;
import java.util.Objects;

import paint.document.model.DocumentModel;
import paint.graphics.model.GraphicalObject;
import paint.model.util.Point;

public class AddShapeState extends IdleState {
	
	private GraphicalObject prototype;
	private DocumentModel model;
	
	public AddShapeState(DocumentModel model, GraphicalObject prototype) {
		this.model = model;
		this.prototype = prototype;
	}

	@Override
	public void mouseDown(Point mousePoint, boolean shiftDown, boolean ctrlDown) {
		Objects.requireNonNull(mousePoint);
		
		GraphicalObject newInstance = prototype.duplicate();
		newInstance.translate(mousePoint); 
		
		model.addGraphicalObject(newInstance);
	}
	
}