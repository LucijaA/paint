package paint.state;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import paint.graphics.model.AbstractGraphicalObject;
import paint.graphics.model.GraphicalObject;
import paint.model.rendering.Renderer;
import paint.model.util.Point;
import paint.model.util.Rectangle;

public class CompositeShape extends AbstractGraphicalObject {

	private List<GraphicalObject> objects;
	
	public CompositeShape(List<GraphicalObject> objects) {
		super(new Point[] {});
		this.objects = objects;
		super.setSelected(true);
	}

	@Override
	public Rectangle getBoundingBox() {
		int xMin = -1;
		int yMin = -1;
		int xMax = 0;
		int yMax = 0;

		for(GraphicalObject obj : objects) {
			Rectangle bb = obj.getBoundingBox();
			
			if(xMin == -1) {
				xMin = bb.getX();
				yMin = bb.getY();
				xMax = bb.getX() + bb.getWidth();
				yMax = bb.getY() + bb.getHeight();
				
				continue;
			}
			
			if(xMin > bb.getX()) xMin = bb.getX();
			if(yMin > bb.getY()) yMin = bb.getY();
			if(xMax < bb.getX() + bb.getWidth()) xMax = bb.getX() + bb.getWidth();
			if(yMax < bb.getY() + bb.getHeight()) yMax = bb.getY() + bb.getHeight();
			
		}
		return new Rectangle(xMin, yMin, xMax - xMin, yMax - yMin);
	}

	@Override
	public double selectionDistance(Point mousePoint) {
		double minSelectionDist = -1;
		
		for (GraphicalObject graphicalObject : objects) {
			double current = graphicalObject.selectionDistance(mousePoint);
			if(minSelectionDist < 0 || current < minSelectionDist) minSelectionDist = current;
		}
		
		return minSelectionDist;
	}


	@Override
	public void render(Renderer r) {
		objects.forEach(o -> o.render(r));
	}

	@Override
	public void translate(Point delta) {
		objects.forEach(o -> o.translate(delta));
		super.notifyListeners();
	}

	@Override
	public void setSelected(boolean selected) {
		objects.forEach(o -> o.setSelected(selected));
		super.setSelected(selected);
	}

	@Override
	public boolean isComposite() {
		return true;
	}
	
	public List<GraphicalObject> getChildren(){
		return objects;
	}

	@Override
	public String getShapeName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GraphicalObject duplicate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getShapeID() {
		return "@COMP";
	}

	@Override
	public void load(Stack<GraphicalObject> stack, String data) {
		int numOfChildren = Integer.parseInt(data.trim());
		
		List<GraphicalObject> children = new ArrayList<>();
		
		while(numOfChildren > 0) {
			children.add(stack.pop());
			
			numOfChildren--;
		}
		
		stack.push(new CompositeShape(children));
		
	}

	@Override
	public void save(List<String> rows) {
		
		objects.forEach(o -> o.save(rows));
		
		StringBuilder sb = new StringBuilder();
		sb.append(getShapeID()).append(" ").append(objects.size());
		
		rows.add(sb.toString());
		
	}
}
