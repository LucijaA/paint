package paint.state;
import java.util.ArrayList;
import java.util.List;

import paint.document.model.DocumentModel;
import paint.graphics.model.GraphicalObject;
import paint.model.rendering.Renderer;
import paint.model.util.Point;

public class EraserState extends IdleState {
	
	private List<GraphicalObject> toDelete;
	private DocumentModel doc;
	private List<Point> points;
	

	public EraserState(DocumentModel doc) {
		super();
		this.doc = doc;
		this.toDelete = new ArrayList<>();
		points = new ArrayList<>();
	}

	@Override
	public void mouseDragged(Point mousePoint) {
		for (GraphicalObject graphicalObject : doc.list()) {
			double distance = graphicalObject.selectionDistance(mousePoint);
			if(distance < 1) toDelete.add(graphicalObject);
		}
		points.add(mousePoint);
		
		doc.notifyListeners();
	}
	
	@Override
	public void mouseDown(Point mousePoint, boolean shiftDown, boolean ctrlDown) {
		points.add(mousePoint);
	}
	
	@Override
	public void mouseUp(Point mousePoint, boolean shiftDown, boolean ctrlDown) {
		toDelete.forEach(o -> {
			doc.removeGraphicalObject(o);
		});
		toDelete.clear();
		points.clear();
		doc.notifyListeners();
	}

	@Override
	public void afterDraw(Renderer r) {
		if(points.isEmpty()) return;
		
		Point first = points.get(0);
		
		for (int i = 1, size = points.size(); i < size; i++) {
			Point second = points.get(i);
			r.drawLine(first, second);
			
			first = second;
		}
	}

}
