package paint.state;
import paint.graphics.model.GraphicalObject;
import paint.model.rendering.Renderer;
import paint.model.util.Point;

public class IdleState implements State {

	@Override
	public void mouseDown(Point mousePoint, boolean shiftDown, boolean ctrlDown) {}

	@Override
	public void mouseUp(Point mousePoint, boolean shiftDown, boolean ctrlDown) {}

	@Override
	public void mouseDragged(Point mousePoint) {}

	@Override
	public void keyPressed(int keyCode) {}

	@Override
	public void afterDraw(Renderer r, GraphicalObject go) {}

	@Override
	public void afterDraw(Renderer r) {}

	@Override
	public void onLeaving() {}

}
