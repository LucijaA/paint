package paint.state;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import paint.document.model.DocumentModel;
import paint.graphics.model.GraphicalObject;
import paint.model.rendering.Renderer;
import paint.model.util.Point;
import paint.model.util.Rectangle;

public class SelectShapeState implements State {
	
	private static final int HOT_POINT_SIZE = 5;
	
	private DocumentModel doc;
	
	
	public SelectShapeState(DocumentModel doc) {
		super();
		this.doc = doc;
	}

	@Override
	public void mouseDown(Point mousePoint, boolean shiftDown, boolean ctrlDown) {
		GraphicalObject obj = doc.findSelectedGraphicalObject(mousePoint);

		if(obj != null && obj.isSelected() && doc.getSelectedObjects().size() == 1) {
			int index = doc.findSelectedHotPoint(obj, mousePoint);
			if(index != -1) {
				obj.setHotPointSelected(index, true);
			}
		} else if(ctrlDown) {
			if(obj == null) return;
			obj.setSelected(true);
			
		} else {
			List<GraphicalObject> toDeselect = new ArrayList<>();
			for (GraphicalObject graphicalObject : doc.getSelectedObjects()) {
				if(graphicalObject.isSelected()) toDeselect.add(graphicalObject);
			}
			
			toDeselect.forEach(gob -> gob.setSelected(false));
			if(obj == null) return;
			obj.setSelected(true);
		}
		
	}

	@Override
	public void mouseUp(Point mousePoint, boolean shiftDown, boolean ctrlDown) {
		if(doc.getSelectedObjects().size() != 1) return;
		
		GraphicalObject obj = doc.getSelectedObjects().get(0);

		int numOfHotPoints = obj.getNumberOfHotPoints();
		for (int i = 0; i < numOfHotPoints; i++) {
			if(obj.isHotPointSelected(i)) obj.setHotPointSelected(i, false);
		}

	}

	@Override
	public void mouseDragged(Point mousePoint) {
		if(doc.getSelectedObjects().size() != 1) return;
		
		GraphicalObject obj = doc.getSelectedObjects().get(0);
		int numOfHotPoints = obj.getNumberOfHotPoints();

		for (int i = 0; i < numOfHotPoints; i++) {
			if(obj.isHotPointSelected(i)) {
				obj.setHotPoint(i, mousePoint);
			}
		}
	}

	@Override
	public void keyPressed(int keyCode) {
		if(doc.getSelectedObjects().isEmpty()) return;
		
		switch(keyCode) {
		
		case KeyEvent.VK_LEFT:
			doc.getSelectedObjects().forEach(go -> go.translate(new Point(-1, 0)));
			break;
			
		case KeyEvent.VK_RIGHT:
			doc.getSelectedObjects().forEach(go -> go.translate(new Point(1, 0)));
			break;
			
		case KeyEvent.VK_UP:
			doc.getSelectedObjects().forEach(go -> go.translate(new Point(0, -1)));
			break;
			
		case KeyEvent.VK_DOWN:
			doc.getSelectedObjects().forEach(go -> go.translate(new Point(0, 1)));
			break;
			
		case KeyEvent.VK_PLUS:
			if(doc.getSelectedObjects().size() == 1) {
				GraphicalObject go = doc.getSelectedObjects().get(0);
				doc.increaseZ(go);
			}
			break;
		case KeyEvent.VK_MINUS:
			if(doc.getSelectedObjects().size() == 1) {
				GraphicalObject go = doc.getSelectedObjects().get(0);
				doc.decreaseZ(go);
			}
			break;
		case KeyEvent.VK_G:
			if(doc.getSelectedObjects().size() > 1) {
				List<GraphicalObject> objects = new ArrayList<>();
				doc.getSelectedObjects().forEach(o -> objects.add(o));
				
				objects.forEach(o -> doc.removeGraphicalObject(o));
				
				CompositeShape comp = new CompositeShape(objects);
				doc.addGraphicalObject(comp);
			}
			break;
		case KeyEvent.VK_U:
			if(doc.getSelectedObjects().size() == 1) {
				GraphicalObject comp = doc.getSelectedObjects().get(0);
				
				if(!comp.isComposite()) break;
				
				CompositeShape composite = (CompositeShape)comp;
				doc.removeGraphicalObject(comp);
				
				List<GraphicalObject> objects = composite.getChildren();
				objects.forEach(o -> doc.addGraphicalObject(o));
			}
			break;
		}
		
	}

	@Override
	public void afterDraw(Renderer r, GraphicalObject go) {
		if(go.isSelected()) {
			Rectangle bb = go.getBoundingBox();
			Point A = new Point(bb.getX(), bb.getY());
			Point B = new Point(bb.getX(), bb.getY() + bb.getHeight());
			Point C = new Point(bb.getX() + bb.getWidth(), bb.getY() + bb.getHeight());
			Point D = new Point(bb.getX() + bb.getWidth(), bb.getY());
			
			r.drawLine(A, B);
			r.drawLine(B, C);
			r.drawLine(C, D);
			r.drawLine(D, A);
		}
	}

	@Override
	public void afterDraw(Renderer r) {
		if(doc.getSelectedObjects().size() == 1) {
			GraphicalObject go = doc.getSelectedObjects().get(0);
			int numOfHotPoints = go.getNumberOfHotPoints();
			
			for (int i = 0; i < numOfHotPoints; i++) {
				Point hotPoint = go.getHotPoint(i);
				
				Point A = new Point(hotPoint.getX() - HOT_POINT_SIZE, hotPoint.getY() - HOT_POINT_SIZE);
				Point B = new Point(hotPoint.getX() - HOT_POINT_SIZE, hotPoint.getY() + HOT_POINT_SIZE);
				Point C = new Point(hotPoint.getX() + HOT_POINT_SIZE, hotPoint.getY() + HOT_POINT_SIZE);
				Point D = new Point(hotPoint.getX() + HOT_POINT_SIZE, hotPoint.getY() - HOT_POINT_SIZE);
				
				r.drawLine(A, B);
				r.drawLine(B, C);
				r.drawLine(C, D);
				r.drawLine(D, A);
			}
		}
	}

	@Override
	public void onLeaving() {
		List<GraphicalObject> toDeselect = new ArrayList<>();
		for (GraphicalObject graphicalObject : doc.getSelectedObjects()) {
			if(graphicalObject.isSelected()) toDeselect.add(graphicalObject);
		}
		
		toDeselect.forEach(gob -> gob.setSelected(false));
	}

}
